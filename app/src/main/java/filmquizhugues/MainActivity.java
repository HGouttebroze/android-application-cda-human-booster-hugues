package filmquizhugues;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.os.PersistableBundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.filmquizhugues.CheatActivity;
import com.example.filmquizhugues.R;

import java.util.ArrayList;
import java.util.List;

import filmquizhugues.pojos.Question;

public class MainActivity extends AppCompatActivity {


    // log
    //private final String TAG = "QuizActivity";
    private final String KEY_QUESTION_EN_COURS = "questionEnCours";
    private  final String KEY_SCORE = "score";
    private final String KEY_TRUE_VISIBILITY = "btnTrue";
    private final String KEY_FALSE_VISIBILITY = "btnFalse";
    private final String KEY_REJOUER_VISIBILITY = "btnRejouer";
    public static final String KEY_QUESTION = "question";

    private Context context;
    private TextView tvQuestion;
    private TextView tvScore;
    private Button btnTrue;
    private Button btnFalse;
    private Button btnRejouer;

    // liste pour stocker les questions
    private List<Question> questions = new ArrayList<>();

    // index de la question encours
    private int questionEncours = 0;
    // objet pour la question encours
    private Question question;
    // score
    private int score = 0;

    private static final String TAG = "QuizActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); // on appel la methode de la classe mere
        setContentView(R.layout.activity_main); // use layout

        // récupère le context de l'application
        context = getApplicationContext();

        // récupère les éléments de la vue sous forme d'objet
        tvQuestion = findViewById(R.id.tvQuestion);
        tvScore = findViewById(R.id.tvScore);
        btnTrue = findViewById(R.id.btnTrue);
        btnFalse = findViewById(R.id.btnFalse);
        btnRejouer = findViewById(R.id.btnRejouer);

        // crée les questions
        Question question1 = new Question(getString(R.string.question_ai), true);
        Question question2 = new Question(getString(R.string.question_taxi_driver), true);
        Question question3 = new Question(getString(R.string.question_2001), false);
        Question question4 = new Question(getString(R.string.question_reservoir_dogs), true);
        Question question5 = new Question(getString(R.string.question_citizen_kane), false);

        // ajoute les questions à la liste
        questions.add(question1);
        questions.add(question2);
        questions.add(question3);
        questions.add(question4);
        questions.add(question5);

        // sauvegarde
        if(savedInstanceState != null){
            questionEncours = savedInstanceState.getInt(KEY_QUESTION_EN_COURS);
            score = savedInstanceState.getInt(KEY_SCORE);
            btnTrue.setVisibility(savedInstanceState.getInt(KEY_TRUE_VISIBILITY));
            btnFalse.setVisibility(savedInstanceState.getInt(KEY_FALSE_VISIBILITY));
            btnRejouer.setVisibility(savedInstanceState.getInt(KEY_REJOUER_VISIBILITY));
        }
        // récupère la question encours
        question = questions.get(questionEncours);

        // met à jour le texte de la question
        tvQuestion.setText(question.getText());

        // met à jour le texte du score
        tvScore.setText(String.format("Score : %d", score));


        // gère le clic sur le bouton btnTrue
        btnTrue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkQuestion(true);

                // test si on n'est pas à la dernière question
                if (questionEncours < questions.size() - 1) {



                    nextQuestion();
                }
                else {
                    btnRejouer.setVisibility(View.VISIBLE);
                    btnTrue.setVisibility(View.INVISIBLE);
                    btnFalse.setVisibility(View.INVISIBLE);
                }

                // met à jour le texte du score
                tvScore.setText(String.format("Score : %d", score));
            }
        });

        // gère le clic sur le bouton btnFalse
        btnFalse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkQuestion(false);

                // test si on n'est pas à la dernière question
                if (questionEncours < questions.size() - 1) {



                    nextQuestion();
                }
                else {
                    btnRejouer.setVisibility(View.VISIBLE);
                    btnTrue.setVisibility(View.INVISIBLE);
                    btnFalse.setVisibility(View.INVISIBLE);
                }

                // met à jour le texte du score
                tvScore.setText(String.format("Score : %d", score));
            }
        });

        // gère le clic sur le bouton btnRejouer
        btnRejouer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // met à 0 l'index de la question
                questionEncours = 0;

                // met à 0 le score
                score = 0;

                // met à jour le texte du score
                tvScore.setText(String.format("Score : %d", score));

                // récupère la question encours
                question = questions.get(questionEncours);

                // met à jour le texte de la question
                tvQuestion.setText(question.getText());

                btnRejouer.setVisibility(View.INVISIBLE);
                btnTrue.setVisibility(View.VISIBLE);
                btnFalse.setVisibility(View.VISIBLE);
            }
        });
    }


    // factorise la vérification de la réponse avec la question posée
    private void checkQuestion(boolean reponse) {

        if (question.isAnswer() == reponse) {
            score++;

            // créé le toast et l'affiche
            Toast toast = Toast.makeText(context, "Bonne réponse", Toast.LENGTH_SHORT);
            toast.show();
        }
        else {
            // créé le toast et l'affiche
            Toast toast = Toast.makeText(context, "Mauvaise réponse", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    // factorise pour passer à la question suivante
    private void nextQuestion() {
        // passe à la question suivante
        questionEncours++;

        // récupère la question encours
        question = questions.get(questionEncours);

        // met à jour le texte de la question
        tvQuestion.setText(question.getText());
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart() called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause() called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume() called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop() called");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart() called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() called");
    }

    @Override // méthode de stockage de mémoire, y aller doucement!!! (on a créé des const...
    public void onSaveInstanceState(@NonNull Bundle outState) {   // , @NonNull PersistableBundle outPersistentState
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState() called");
        outState.putInt(KEY_QUESTION_EN_COURS, questionEncours); // clé => valeur, permet d'enregistrer des clés en mémoire avec l'objet bundle
        outState.putInt(KEY_SCORE, score);
        outState.putInt(KEY_TRUE_VISIBILITY, btnTrue.getVisibility());
        outState.putInt(KEY_FALSE_VISIBILITY, btnFalse.getVisibility());
        outState.putInt(KEY_REJOUER_VISIBILITY, btnRejouer.getVisibility());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // effectue 1 action suivant l'item select
        switch (item.getItemId()) {
            case R.id.cheat:
                // créé un Intent pr ensuite lancer CheatActivity
                Intent intent = new Intent(context, CheatActivity.class); // on cree un itent , objet qui permet de démarer 1 new activity
                         // on check l'attribut content qui appel la méthode getApplicationContext() context déclaré plus haut

                // add datas ds le intent
                intent.putExtra(KEY_QUESTION, question); // ! il faut rendre la Class serializable (implements Serializable)

                startActivity(intent); // on appel la methode startActivity avec item en attribut pr démarrer l'activité


                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

