package filmquizhugues.pojos;

import java.io.Serializable;

public class Question implements Serializable { // on rend le pojo serializable, on enregistre id, text & answer, puis recreation de l objet pour deserializer (unserializable)

    private int id;
    private String text;
    private  boolean answer;

    public Question() {

    }

    public Question(String text, boolean answer) {
        this.text = text;
        this.answer = answer;
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public boolean isAnswer() {
        return answer;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setAnswer(boolean answer) {
        this.answer = answer;
    }

}
