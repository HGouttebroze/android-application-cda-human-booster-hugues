package com.example.filmquizhugues;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import filmquizhugues.MainActivity;
import filmquizhugues.pojos.Question;

public class CheatActivity extends AppCompatActivity {

    private TextView tvReponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheat);

        tvReponse = findViewById(R.id.tvReponse);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // check l'intent qui appel cette activité
        Intent intent = getIntent();

        // check question ds intent
        Question question = (Question)intent.getSerializableExtra(MainActivity.KEY_QUESTION); // deserialize l'objet & on caste car c'est un objet

        tvReponse.setText(String.format("%s : %s", question.getText(), question.isAnswer()));

    }

    @Override
    public boolean onSupportNavigateUp() {
        // termine 1 activité
        finish();
        return true;
    }
}