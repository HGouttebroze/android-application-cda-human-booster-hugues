package com.example.filmquizhugues;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

//public class MainActivity<Question> extends AppCompatActivity {
public class MainActivity extends AppCompatActivity {

    private Button btnTrue;
    private Button btnFalse;

    // liste pr stocker les questions
    //private List<Question> questions = new ArrayList<>();

    //private static final String TAG = "QuizActivity";

    // creation fonction appel question text avec list

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); // on appel la methode de la classe mere
        setContentView(R.layout.activity_main); // use layout

        // on recupère les éléments de la vue avec le code ci-dessous
        btnTrue = findViewById(R.id.btnTrue);
        btnFalse = findViewById(R.id.btnFalse);

        // exemple pour acceder aux ressourses puis renseigner l'objet (sauf l'id)
       // String text = getString(R.string.question);
        //String answer = getString(R.string.question_2001);

        //if(text == answer) {

        //};
        //String[] questions = res.getStringArray(R.array.question_array);

        /////////////////////////////////////
        // gère le click sur le btn btnTrue
        btnTrue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // recup context application
                Context context = getApplicationContext();

                //CharSequence text = "Hey Oh Let's Go !";

                // creation du toast et affichage
                Toast toast = Toast.makeText(context, "VRAI", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

        btnFalse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Context context = getApplicationContext();

                //CharSequence text = "Hey Oh Let's Go !";

                Toast toast = Toast.makeText(context, "FAUX", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

    }

}