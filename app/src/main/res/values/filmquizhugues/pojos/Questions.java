package com.example.filmquizhugues.pojos;

public class Questions {

    private int id;
    private String text;
    private  boolean answer;

    public Questions() {

    }

    public Questions(String text, boolean answer) {
        this.text = text;
        this.answer = answer;
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public boolean isAnswer() {
        return answer;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setAnswer(boolean answer) {
        this.answer = answer;
    }

}
